<?php

namespace Drupal\vb_mailjet\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Mailjet\Client as MJClient;
use Mailjet\Resources as MJResources;

/**
 * Submits .
 *
 * @WebformHandler(
 *   id = "mailjet_webform_handler",
 *   label = @Translation("Mailjet Webform Handler"),
 *   category = @Translation("Transaction"),
 *   description = @Translation("Sends the submission data to Mailjet."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class MailjetWebformHandler extends WebformHandlerBase {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'list_id' => '',
      'email' => '',
      'name' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $mj_config = \Drupal::config('vb_mailjet.settings');
    $mj = new MJClient($mj_config->get('api_key'), $mj_config->get('api_secret'));
    $form['list_id'] = [
      '#type' => 'select',
      '#title' => $this->t('List'),
      '#options' => ['' => $this->t('Select a list')],
      '#weight' => 0,
      '#required' => TRUE,
    ];
    $result = $mj->get(MJResources::$Contactslist, ['filters' => ['IsDeleted' => FALSE, 'Sort' => 'Name', 'Limit' => 1000]]);
    if($result->success()) {
      foreach($result->getBody()['Data'] as $list) {
        $form['list_id']['#options'][$list['ID']] = $list['Name'];
      }
    }
    if(isset($form['list_id']['#options'][$this->configuration['list_id']])) {
      $form['list_id']['#default_value'] = $this->configuration['list_id'];
    }
    if(!empty($this->configuration['list_id'])) {
      $elements = $this->webform->getElementsDecodedAndFlattened();
      foreach($elements as $key => $element) {
        if(!in_array($element['#type'], ['textfield', 'select', 'email', 'hidden'])) {
          unset($elements[$key]);
        }
      }
      $options = [];
      foreach($elements as $key => $element) {
        $options[$key] = $element['#title'];
      }
      $form['email'] = [
        '#type' => 'select',
        '#title' => $this->t('Email'),
        '#options' => $options,
        '#required' => TRUE,
        '#default_value' => $this->configuration['email'],
      ];
      $form['name'] = [
        '#type' => 'select',
        '#title' => $this->t('Full name'),
        '#options' => $options,
        '#empty_value' => '',
        '#default_value' => $this->configuration['name'],
      ];
      $result = $mj->get(MJResources::$Contactmetadata, ['filters' => ['DataType' => 'str', 'Sort' => 'Name']]);
      if($result->success()) {
        foreach($result->getBody()['Data'] as $response) {
          $key = 'metadata_'.$response['ID'];
          $form[$key] = [
            '#type' => 'select',
            '#title' => $response['Name'],
            '#options' => $options,
            '#empty_value' => '',
            '#default_value' => $this->configuration[$key],
          ];
        }
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['list_id'] = $form_state->getValue('list_id');
    $this->configuration['email'] = $form_state->getValue('email');
    $this->configuration['name'] = $form_state->getValue('name');
    if(!empty($this->configuration['list_id'])) {
      $mj_config = \Drupal::config('vb_mailjet.settings');
      $mj = new MJClient($mj_config->get('api_key'), $mj_config->get('api_secret'));
      $result = $mj->get(MJResources::$Contactmetadata, ['DataType' => 'str']);
      if($result->success()) {
        foreach($result->getBody()['Data'] as $response) {
          $key = 'metadata_'.$response['ID'];
          $this->configuration[$key] = $form_state->getValue($key);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state,  WebformSubmissionInterface $webform_submission) {
    $mj_config = \Drupal::config('vb_mailjet.settings');
    $values = $webform_submission->getData();
    $mj = new MJClient($mj_config->get('api_key'), $mj_config->get('api_secret'));
    $result = $mj->get(MJResources::$Contactmetadata, ['DataType' => 'str']);
    $keys = [];
    if($result->success()) {
      foreach($result->getBody()['Data'] as $response) {
        $keys[$response['Name']] = 'metadata_'.$response['ID'];
      }
    }
    $data['body'] = [
      'Email' => $values[$this->configuration['email']],
      'IsExcludedFromCampaigns' => FALSE,
    ];
    if(!empty($this->configuration['name'])) {
      $data['body']['Name'] = $values[$this->configuration['name']];
    }
    $result = $mj->get(MJResources::$Contact, ['id' => $values[$this->configuration['email']]]);
    if(!$result->success() || $result->getBody()['Count'] == 0) {
      $result = $mj->post(MJResources::$Contact, $data);
    }
    if($result->success()) {
      $data['id'] = $result->getBody()['Data'][0]['ID'];
      $data['body'] = [];
      $data['body']['ContactsLists'][] = [
        'ListID' => $this->configuration['list_id'],
        'Action' => 'addforce', // explicitly consented
      ];
      $result = $mj->post(MJResources::$ContactManagecontactslists, $data);
      $data['body'] = [];
      foreach($keys as $cm_key => $key) {
        if(!empty($values[$this->configuration[$key]])) {
          $data['body']['Data'][] = [
            'Name' => $cm_key,
            'Value' => $values[$this->configuration[$key]],
          ];
        }
      }
      if(count($data['body'])) {
        $mj->put(MJResources::$Contactdata, $data);
      }
    }
  }
}
